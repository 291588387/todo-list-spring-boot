package com.todoList.demo;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.todoList.demo.entity.Todo;
import com.todoList.demo.repository.TodoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@SpringBootTest
@AutoConfigureMockMvc
public class TodoIntegrationTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    TodoRepository todoRepository;

    @BeforeEach
    void setUp() {
        todoRepository.deleteAll();
    }

    @Test
    void should_get_all_todos() throws Exception {
        Todo todo = getOneTodo();
        todoRepository.save(todo);
        mockMvc.perform(get("/todos"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(todoRepository.findAll().size()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].text").value(todo.getText()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].done").value(todo.isDone()));
    }

    @Test
    void should_add_todo_given_todo() throws Exception {
        Todo todo = getOneTodo();

        ObjectMapper objectMapper = new ObjectMapper();
        String employeeRequestJSON = objectMapper.writeValueAsString(todo);
        mockMvc.perform(post("/todos")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(employeeRequestJSON))
                .andExpect(MockMvcResultMatchers.status().is(201))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.text").value(todo.getText()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.done").value(todo.isDone()));
    }

    @Test
    void should_return_not_acceptted_when_add_todo_given_empty_text() throws Exception {
        Todo todo = new Todo(null, "   ", false);

        ObjectMapper objectMapper = new ObjectMapper();
        String employeeRequestJSON = objectMapper.writeValueAsString(todo);
        mockMvc.perform(post("/todos")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(employeeRequestJSON))
                .andExpect(MockMvcResultMatchers.status().is(406))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("Todo text can not be empty"));
    }

    @Test
    void should_delete_todo_given_todo_id() throws Exception {
        Todo todo = getOneTodo();
        todoRepository.save(todo);

        mockMvc.perform(delete("/todos/{id}", todo.getId()))
                .andExpect(MockMvcResultMatchers.status().is(204));

        assertTrue(todoRepository.findById(todo.getId()).isEmpty());
    }

    @Test
    void should_update_todo_given_id_and_todo() throws Exception {
        Todo previousTodo = new Todo(null, "abcdefg", false);
        todoRepository.save(previousTodo);

        Todo todoUpdateRequest = new Todo(null,"abcd", true);
        ObjectMapper objectMapper = new ObjectMapper();
        String updatedEmployeeJson = objectMapper.writeValueAsString(todoUpdateRequest);
        mockMvc.perform(put("/todos/{id}", previousTodo.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(updatedEmployeeJson))
                .andExpect(MockMvcResultMatchers.status().is(200));

        Optional<Todo> optionalTodo = todoRepository.findById(previousTodo.getId());
        assertTrue(optionalTodo.isPresent());
        Todo updatedTodo = optionalTodo.get();
        Assertions.assertEquals(todoUpdateRequest.isDone(), updatedTodo.isDone());
        Assertions.assertEquals(todoUpdateRequest.getText(), updatedTodo.getText());
    }

    @Test
    void should_get_todo_by_id_given_id() throws Exception {
        Todo todo = getOneTodo();
        todoRepository.save(todo);

        mockMvc.perform(get("/todos/{id}", todo.getId()))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(todo.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.text").value(todo.getText()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.done").value(todo.isDone()));
    }

    @Test
    void should_return_not_found_when_find_todo_by_id() throws Exception {
        mockMvc.perform(get("/todos/{id}", 99999))
                .andExpect(MockMvcResultMatchers.status().is(404))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("Todo Not Found"))
        ;
    }

    private Todo getOneTodo() {
        Todo todo = new Todo();
        todo.setText("额UR与回复可见的实力发生分手费");
        todo.setDone(false);
        return todo;
    }

}
