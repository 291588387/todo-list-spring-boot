package com.todoList.demo.service;

import com.todoList.demo.entity.Todo;
import com.todoList.demo.exception.EmptyTextException;
import com.todoList.demo.exception.TodoNotFoundException;
import com.todoList.demo.repository.TodoRepository;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Objects;

@Service
public class TodoService {
    private final TodoRepository todoRepository;


    public TodoService(TodoRepository todoRepository) {
        this.todoRepository = todoRepository;
    }

    public List<Todo> getAllTodos() {
        return todoRepository.findAll();
    }

    public Todo addTodo(Todo todo) {
        if (StringUtils.isEmpty(todo.getText().trim())) throw new EmptyTextException();
        return todoRepository.save(todo);
    }

    public void deleteTodo(Long id) {
        todoRepository.deleteById(id);
    }

    public Todo findById(Long id) {
        return todoRepository.findById(id).orElseThrow(TodoNotFoundException::new);
    }

    public Todo updateTodo(Long id, Todo todo) {
        Todo targetTodo = findById(id);
        targetTodo.setText(Objects.nonNull(todo.getText()) ? todo.getText() : targetTodo.getText());
        targetTodo.setDone(todo.isDone());
        return todoRepository.save(targetTodo);
    }
}
