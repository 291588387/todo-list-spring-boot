package com.todoList.demo.exception;

public class TodoNotFoundException extends RuntimeException{
    public TodoNotFoundException() {
        super("Todo Not Found");
    }
}
