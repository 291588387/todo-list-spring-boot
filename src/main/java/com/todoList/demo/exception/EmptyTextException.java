package com.todoList.demo.exception;

public class EmptyTextException extends RuntimeException {
    public EmptyTextException() {
        super("Todo text can not be empty");
    }
}
