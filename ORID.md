## O:

- Learned about side effect and solved confusion on useRef hook
- Managed to solve cross origin resource sharing problem when implementing todoList backend.
- Drew concept map of frontend to have better understandings.
- Held our presentation on Elevator speech and Minimum viable product.;

## R:

- Fruitful

## I:

- Felt quite confident this time in presentation since I'm responsible for the acting at the beginning
- Felt well benefited as I went through the integration of frontend and backend by myself

## D:

- Improve influence in presentation 


